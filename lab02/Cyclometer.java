// Matt Solomon 
// 9/5/18
// CSE 2 
// Program will print out the minutes and counts for each trip as well as the distance of the two trips in miles 

public class Cyclometer {
  
  public static void main (String[] args){
    int secsTrip1 = 480;  // how many seconds it takes for the first trip 
    int secsTrip2 = 3220; // how many seconds it takes for the second trip 
    int countsTrip1 = 1561; // how many counts it takes for the first trip 
    int countsTrip2 = 9037; // how many counts it takes for the second trip
    
    double wheelDiameter = 27.0, //diameter of the wheel 
    PI =3.14159, //number for pi 
    feetPerMile = 5280, // number of feet in a mile 
    inchesPerFoot = 12, //number of inches in a foot 
    secondsPerMinute = 60;//number of seconds in a minute 
    double distanceTrip1, distanceTrip2, totalDistance;  // distance for each trip as well as the total distance
    
    System.out.println ("Trip 1 took "+ 
                       (secsTrip1/secondsPerMinute) +" minutes and had "+ 
                       countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
                      (secsTrip2/ secondsPerMinute)+" minutes and had "+
                      countsTrip2+" counts.");
    //
    //
    //
    //
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    // above gives distance in inches 
    // for each count, a rotation of the wheel travels 
    //the diameter in inches times PI
    distanceTrip1/= inchesPerFoot * feetPerMile; // gives distance in miles 
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot/ feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    
    System.out.println("Trip 1 was "+ distanceTrip1 +" miles");
    System.out.println("Trip 2 was "+ distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
      
    
  }//end of main method 
}//end of class