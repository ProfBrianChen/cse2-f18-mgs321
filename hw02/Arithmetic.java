// Matt Solomon
// hw02
// 9/11/18
//  Program will calculate the cost of various items bought at a store

public class Arithmetic{
  public static void main(String args[]){
    
    // number of pairs of pants 
    int numPants = 3;
    // cost per pair of pants 
    double pantsPrice = 34.98;
    
    // number of sweatshirts 
    int numShirts = 2;
    // cost per shirt 
    double shirtPrice = 24.99;
    
    // number of belts 
    int numBelts = 1;
    //cost per belt 
    double beltCost = 33.99;
    
    // tax rate 
    double paSalesTax = 0.06;
    
    // total cost of pants 
    double totalCostPants = (numPants * pantsPrice) + (paSalesTax * (numPants * pantsPrice));
    
    // total cost of sweatshirts 
    double totalCostShirts = (numShirts * shirtPrice) + (paSalesTax * (numShirts * shirtPrice));
    
    //total cost of belts
   double totalCostBelts = (numBelts * beltCost) + (paSalesTax * (numBelts * beltCost));
    
    // sales tax charged for pants, shirts and belts 
    double taxPants = (paSalesTax * (numPants * pantsPrice));
   double taxShirts = (paSalesTax * (numShirts * shirtPrice));
    double taxBelts = (paSalesTax * (numBelts * beltCost));
                
    // total cost of purchases before taxes 
    double totalPantsBefTaxes = (numPants * pantsPrice);
    double totalShirtsBefTaxes = (numShirts * shirtPrice);
    double totalBeltsBefTaxes = (numBelts * beltCost);
                
    // total sales tax 
    double totalSalesTax = taxPants + taxShirts + taxBelts;
    
    // total paid for transaction including sales tax 
   double totalCost = totalCostPants + totalCostShirts + totalCostBelts;
                       
  System.out.println("The total cost of pants was "+ totalCostPants +" dollars.");
   System.out.println("The total cost of shirts was " + totalCostShirts +" dollars");
   System.out.println("The total sales tax for pants was " + taxPants +".");
   System.out.println("The total sales tax for shirts was " + taxShirts +".");
   System.out.println("The total sales tax for belts was " + taxBelts +".");
   System.out.println("The total cost of belts was " + totalCostBelts +" dollars");
   System.out.println("The total cost of pants before taxes was " + totalPantsBefTaxes +" dollars");
   System.out.println("The total cost of shirts before taxes was " + totalShirtsBefTaxes +" dollars");
   System.out.println("The total cost of belts before taxes was " + totalBeltsBefTaxes +" dollars");
   System.out.println("The total sales tax was " + totalSalesTax+".");
   System.out.println("The total transaction cost " + totalCost+".");
                
    
    
      
  }  
    
  
}