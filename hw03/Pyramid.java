
// Matt Solomon
// CSE 2
// 9/18/18
// The program will ask the user for the dimensions of a pyramid and then will print out the volume of the pyramid.
import java.util.Scanner;
public class Pyramid{
  public static void main(String args []){
    Scanner myScanner = new Scanner(System.in);
    
    System.out.print("The length of the pyramid is (imput length in decimal form): "); // first prompt for user
    double lengthPyramid = myScanner.nextDouble();
    System.out.print("The height of the pyramid is (input height in decimal form): "); // second prompt for user
    double heightPyramid = myScanner.nextDouble();
    System.out.print("The width of the pyramid is (input length in decimal form): "); // third prompt for user
   
    double widthPyramid = myScanner.nextDouble();
    double volPyramid;
    volPyramid = (lengthPyramid * heightPyramid * widthPyramid) / 3; // equation for the volume of a pyramid 
   
    System.out.print("The volume of the pyramid is " + volPyramid + "."); // final answer that the user will see
    
    
  }
}
