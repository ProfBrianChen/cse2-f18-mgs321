// Matt Solomon
// Hw03 
// 9/18/18
// This program tells the user how much rain was dropped on a certain amount of land.

import java.util.Scanner;
public class Convert{
  public static void main(String args []){
    Scanner myScanner = new Scanner(System.in);
    
    System.out.print("Enter the affected area in acres: "); // this is the first prompt that the user will see
    double numAcres = myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affected area: "); // the second prompt that the user will see
    int amountRain = myScanner.nextInt();
    
    double acresInch = numAcres * amountRain;
    double gallons = acresInch * 27154.2857;
    double cubicMiles = gallons * 9.08169e-13;
    // these three lines are converting acres-inch to gallons and then gallons to cubic miles
    
    System.out.print(cubicMiles + " " + "cubic miles."); // gives the final answer in cubic miles
  }
}




