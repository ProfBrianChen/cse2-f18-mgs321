// Matt Solomon 
// lab 5 
// the program will prompt the user to enter information about a course.  
// if they enter the correct type of text it will go to the next question.
// if not it will give the user an error and prompt them to try again.

import java.util.Scanner; 
public class Input{ 
	public static void main(String args []){ 
    Scanner myScanner = new Scanner(System.in); 
    String teachName; 
    int meetTime; 
    int numMeets; 
    String departName; 
    int courseNum; 
    int numStudents; 
     
    System.out.println("Enter course number: "); //prompts user to enter course number
    while (true) { 
			if (myScanner.hasNextInt()) { 
      	courseNum = myScanner.nextInt(); // if they enter an int it will move on
        break; 
      } 
      else { 
        myScanner.nextLine(); 
        System.out.println("ERROR: Try again. "); // if not an int program will give this message 
      } 
		} 
    
    System.out.println("Enter the department name: "); // prompts user to enter department name 
    while (true) {
      if (myScanner.hasNext()) {
        departName = myScanner.next();// if they enter a string it will move on
        break;
      }
      else {
        myScanner.nextLine();
        System.out.println("ERROR: Try again. "); // if not a string program will give this message 
      }
		}
    
    System.out.println("Enter number of times a week the class meets: "); //prompts user to enter the amount of time the class meets
    while (true){
      if (myScanner.hasNextInt()) {
        numMeets = myScanner.nextInt();// if they enter an int it will move on
        break;
      }
      else {
        myScanner.nextLine();
        System.out.println("ERROR: Try again. ");// if not an int program will give this message 
      }
		}
    
    System.out.println("Enter what time the class starts. (XXX format): "); // prompts user to give the time the class starts
    while (true) {
      if (myScanner.hasNextInt()) {
        meetTime = myScanner.nextInt();// if they enter an int it will move on
        break;
      }
      else {
        myScanner.nextLine();
        System.out.println("ERROR: Try again. ");// if not an int program will give this message
      }
    }
    
    System.out.println("Enter the instructors name: ");//will prompt user for the instructors name
		while (true) {
      if (myScanner.hasNext()) {
        teachName = myScanner.next();// if they enter a string it will move on
				break;
      }
      else {
        myScanner.nextLine();
        System.out.println("Error: Try again. "); // if not a string program will give this message
      }
    }
    
    System.out.println("Enter number of students: ");// will prompt user for the number of students
    while (true) {
      if (myScanner.hasNextInt()) {
        numStudents = myScanner.nextInt();// if they enter an int it will move on
        break;
      }
      else {
        myScanner.nextLine();
        System.out.println("Error: Try again. ");// if not an int program will give this message
      }
    }
  }
} 
             


