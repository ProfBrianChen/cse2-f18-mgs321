// Matt Solomon
// CSE 2
// 9/12/18
// the program will print out how much each person in the group that went to dinner has to pay in order to cover the check


import java.util.Scanner;
public class Check{
  public static void main(String [] args){
    Scanner myScanner = new Scanner( System.in);
    
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;
    System.out.print("Enter the amount of people that went out to dinner: ");
    int numPeople = myScanner.nextInt();
    double totalCost;
      double costPerPerson;
      int dollars, dimes, pennies;
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10;
    pennies =(int)(costPerPerson * 100) % 10;
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
    
  }
}