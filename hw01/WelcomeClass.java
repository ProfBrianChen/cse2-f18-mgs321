// Matt Solomon 
//hw01
//   9/4/18
// The program will print out the word "Welcome" and my lehigh id
public class WelcomeClass{
 
  public static void main(String args[]){
    
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-M--G--S--3--2--1->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
    
 

    
}
  