// Matt Solomon
//CSE 2
// hw 4
//program will used nested ifs to determine craps rolls from either a randomly generated roll or a roll where the user chooses.

import java.lang.Math;
import java.util.Scanner;
public class CrapsIf {
  public static void main(String args []){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Do you want to randomly cast dice? Y or N:");
    
    if(myScanner.nextLine().equals("Y")) {
     int num1 = (int)(Math.random() * 6) + 1;
     System.out.println(num1);
     int num2 = (int)(Math.random() * 6) + 1;
     System.out.println(num2);
      if (num1 == 1){
        if (num2 == 1){
          System.out.print("Snake Eyes");
        }
        else if (num2 == 2){
          System.out.print("Ace Deuce");
        }
        else if (num2 == 3){
          System.out.print("Easy Four");
        }
        else if (num2 == 4){
          System.out.print("Fever Five");
        }
        else if (num2 == 5){
          System.out.print("Easy Six");
        }
        else if (num2 == 6){
          System.out.print("Seven Out");
        }
      }
      else if (num1 == 2){
        if (num2 == 2){
          System.out.print("Hard Four");
        }
        else if (num2 == 3){
          System.out.print("Fever Five");
        }
        else if (num2 == 4){
          System.out.print("Easy Six");
        }
        else if (num2 == 5){
          System.out.print("Seven Out");
        }
        else if (num2 == 6){
          System.out.print("Easy Eight");
        }
      }
      else if (num1 == 3){
        if (num2 == 3){
          System.out.print("Hard Six");
        }
        else if (num2 == 4){
          System.out.print("Seven Out");
        }
        else if (num2 == 5){
          System.out.print("Easy Eight");
        }
        else if (num2 == 6){
          System.out.print("Nine");
        }
      }
      else if (num1 == 4){
        if (num2 == 4){
          System.out.print("Hard Eight");
        }
        else if (num2 == 5){
          System.out.print("Nine");
        }
        else if (num2 == 6){
          System.out.print("Easy Ten");
        }
      }
      else if (num1 == 5){
        if (num2 == 5){
          System.out.print("Hard Ten");
        }
        else if (num2 == 6){
          System.out.print("Yo-leven");
        }
      }
      else if (num1 == 6){
        if (num2 == 6){
          System.out.print("Boxcars");
        }

        }
      }
    
    else if (myScanner.nextLine().equals("N")) {
      System.out.print("Please enter the first number.");
      int num1 = myScanner.nextInt();
      System.out.print("Please enter the second number.");
      int num2 = myScanner.nextInt();
      
      if (num1 == 1){
        if (num2 == 1){
          System.out.print("Snake Eyes");
        }
        else if (num2 == 2){
          System.out.print("Ace Deuce");
        }
        else if (num2 == 3){
          System.out.print("Easy Four");
        }
        else if (num2 == 4){
          System.out.print("Fever Five");
        }
        else if (num2 == 5){
          System.out.print("Easy Six");
        }
        else if (num2 == 6){
          System.out.print("Seven Out");
        }
      }
      else if (num1 == 2){
        if (num2 == 2){
          System.out.print("Hard Four");
        }
        else if (num2 == 3){
          System.out.print("Fever Five");
        }
        else if (num2 == 4){
          System.out.print("Easy Six");
        }
        else if (num2 == 5){
          System.out.print("Seven Out");
        }
        else if (num2 == 6){
          System.out.print("Easy Eight");
        }
      }
      else if (num1 == 3){
        if (num2 == 3){
          System.out.print("Hard Six");
        }
        else if (num2 == 4){
          System.out.print("Seven Out");
        }
        else if (num2 == 5){
          System.out.print("Easy Eight");
        }
        else if (num2 == 6){
          System.out.print("Nine");
        }
      }
      else if (num1 == 4){
        if (num2 == 4){
          System.out.print("Hard Eight");
        }
        else if (num2 == 5){
          System.out.print("Nine");
        }
        else if (num2 == 6){
          System.out.print("Easy Ten");
        }
      }
      else if (num1 == 5){
        if (num2 == 5){
          System.out.print("Hard Ten");
        }
        else if (num2 == 6){
          System.out.print("Yo-leven");
        }
      }
      else if (num1 == 6){
        if (num2 == 6){
          System.out.print("Boxcars");
        }
    
      }
    }
             
  }
}