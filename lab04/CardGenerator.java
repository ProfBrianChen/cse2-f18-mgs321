//Matt Solomon
//lab 4
//9/19/18
// the program will randomly generate a number and tell you what card you picked
import java.lang.Math;
public class CardGenerator{
  public static void main(String args []){
    
   int randomNum;
    int suitName;
    int cardNum;
    String cardSuit = "";
    String cardVal = "";
randomNum = (int)(Math.random() * 51) + 1;
    
    
    if ((randomNum >= 1) && (randomNum<= 13)){ // if the card number is between 1 and 13 than the suit is a diamond
     cardSuit = "Diamonds";
    
    }
      else if ((randomNum >= 14) && (randomNum <= 26)){ //if the number is between 14 and 26 the suit will be clubs 
        cardSuit = "Clubs"; 
      
      }
    else if  ((randomNum >= 27) && (randomNum <= 39)){ //if the number is between 27 and 39 the suit is hearts 
      cardSuit = "Hearts";
     
    }
    else if ((randomNum >= 40) && (randomNum<= 52)){ // if the number is between 40 and 52 the suit is spades
      cardSuit = "Spades";
      
    }
    
    cardNum = ((randomNum - 1) % 13) + 1;// calculates card number between 1 and 13 
    
       if( (cardNum >= 2) && (cardNum <= 10)) { //values 2 - 10
      cardVal = "" + cardNum;
    }
    else {
    switch (cardNum){  //1 - A, 11 - J, 12 - Q, 13 - K
                        // will return ace,jack,queen or king for these certain random numbers
      case 1: 
        cardVal = "Ace";
          break;
     
      case 11:
        cardVal = "Jack";
        break;
      
      case 12:
        cardVal = "Queen";
        break;
      
      case 13:
        cardVal = "King";
        break;
     
      default:
        break;
        
    }
    }
 System.out.println("You picked the" + cardVal +  " " + "of" + " " + cardSuit); // prints out the card value and suit 
  }
}